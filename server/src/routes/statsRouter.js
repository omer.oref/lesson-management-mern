const router = require('express').Router()
const statsController = require('../controllers/statsController')
const verifyAreaId = require('../middleware/verifyAreaId')

router.route('/areas/:areaId/stats')
  .get(verifyAreaId, statsController.getStats)

router.route('/areas/:areaId/fieldstats/:field')
  .get(verifyAreaId, statsController.getFieldStats)

module.exports = router