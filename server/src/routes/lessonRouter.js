const router = require('express').Router()
const lessonController = require('../controllers/lessonController')
const verifyAreaId = require('../middleware/verifyAreaId')
const verifyLessonIndex = require('../middleware/verifyLessonIndex')
const verifyAuth = require('../middleware/verifyAuth')

router.route('/areas/:areaId/lessons')
  .get(verifyAreaId, lessonController.getAllLessons)
  .post(verifyAreaId, verifyAuth, lessonController.createNewLesson)

router.route('/areas/:areaId/lessons/:lessonIndex')
  .get(verifyAreaId, verifyLessonIndex, lessonController.getOneLesson)
  .patch(verifyAreaId, verifyLessonIndex, verifyAuth, lessonController.updateOneLesson)
  .put(verifyAreaId, verifyLessonIndex, verifyAuth, lessonController.setOneLesson)
  .delete(verifyAreaId, verifyLessonIndex, verifyAuth, lessonController.deleteOneLesson)

module.exports = router