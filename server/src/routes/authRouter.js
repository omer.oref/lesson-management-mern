const router = require('express').Router()
const authController = require('../controllers/authController')
const verifyAreaId = require('../middleware/verifyAreaId')
const verifyAuth = require('../middleware/verifyAuth')
const rateLimiter = require('../middleware/rateLimiter')

router.route('/auth')
  .get(authController.getAuth)

router.route('/areas/:areaId/auth/login')
  .post(rateLimiter, verifyAreaId, authController.loginArea)

router.route('/auth/master/login')
  .post(rateLimiter, authController.loginMaster)

router.route('/auth/master/register')
  .post(verifyAuth, authController.registerMaster)

router.route('/auth/logout')
  .get(authController.logout)

module.exports = router