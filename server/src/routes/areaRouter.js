const router = require('express').Router()
const areaController = require('../controllers/areaController')
const verifyAreaId = require('../middleware/verifyAreaId')
const verifyAuth = require('../middleware/verifyAuth')

router.route('/areas/')
  .get(areaController.getAllAreas)
  .post(verifyAuth, areaController.createNewArea)

router.route('/areas/:areaId')
  .get(verifyAreaId, areaController.getOneArea)
  .patch(verifyAreaId, verifyAuth, areaController.updateOneArea)
  .delete(verifyAreaId, verifyAuth, areaController.deleteOneArea)

module.exports = router