const router = require('express').Router()
const fieldsController = require('../controllers/fieldsController')
const verifyAreaId = require('../middleware/verifyAreaId')
const verifyAuth = require('../middleware/verifyAuth')

router.route('/areas/:areaId/fields')
  .get(verifyAreaId, fieldsController.getFields)
  .put(verifyAreaId, verifyAuth, fieldsController.setFields)

module.exports = router