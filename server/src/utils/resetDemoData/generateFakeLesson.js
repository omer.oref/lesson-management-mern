module.exports = () => {
  let lesson = {}
  lesson.drill = getDrill()
  lesson.subject = getSubject()
  lesson.mission = getLoremHebrew()
  lesson.dateToFinish = getDate()
  lesson.status = getStatus()
  lesson.responsibility = getResponsibility()
  lesson.debrief = getDebrief()
  lesson.subjectPlus = getLoremHebrew()
  lesson.stone = getStone()
  lesson.statusPlus = getLoremHebrew()
  return lesson
}

function getLoremHebrew() {
  return getRandomFromArray([
    'לורם איפסום דולור סיט אמט, קונסקטורר',
    ' אדיפיסינג אלית קונדימנטום',
    'ורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.',
    'גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט',
    'ליבם סולגק. בראיט ולחת צורק',
    'מונחף, בגורמי מגמש.',
    ' תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.',
    'ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.',
    'לורם איפסום דולור סיט אמט',
    'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור',
    'ליבם סולגק. בראיט ולחת צורק',
    'קראס אגת לקטוס וואל אאוגו',
    'וסטיבולום סוליסי',
    'טידום בעליק. קונדימנטום קורוס בליקרה',
    'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה.',
  ])
}

function getDrill() {
  return getRandomFromArray([
    {
      id: "123",
      name: "מבצע שומר החומות 2021",
      date: "2021-10-05"
    },
    {
      id: "234",
      name: "תרגיל אבן פינה 2021",
      date: "2021-09-06"
    },
    {
      id: "345",
      name: "מבצע קרן אור 2020",
      date: "2020-10-20"
    }
  ])
}

function getDebrief() {
  return getRandomFromArray([
    'הסברה ותקשורת',
    'פו"ש',
  ])
}  

function getSubject() {
  return getRandomFromArray([
    'כללי',
    'אמצעים',
    'אוכלוסייה',
    'הסברה',
  ])
}

function getStone() {
  return getRandomFromArray([
    'ממשק פקע"ר עם מוקדים הרשותיים',
    'מיפוי ואבחון צרכי אוכלוסייה',
    'משפיעי דעת קהל',
    'אחידות המסר',
    'ציר פיקוד אורכי',
    'הפעלת כוחות',
  ])
}

function getDate() {
  let date = randomDate(new Date(2020, 1, 1), new Date(2022, 11, 31))
  return {
    name: date.toLocaleDateString('en-CA'),
    year: date.getFullYear()
  }
}

function getStatus() {
  let statuses = [
    'בוצע',
    'לא בוצע',
    'בביצוע',
  ]  
  return getRandomFromArray(statuses)
}

function getResponsibility() {
  return getRandomArrFromArray([
    'רמ"ח אוכ',
    'משקא"פ',
    'מפקדי מרכזים',
    'רע"ן תוהד',
    'רע"ן אגמים',
    'רע"ן עורף'
  ])
}


//-- utilities --//

function getRandomInt(min, max, exclude=[]) {
  let randomInt = Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + min;
  while(exclude.includes(randomInt)) {
    randomInt = Math.floor(Math.random() * (Math.floor(max) - Math.ceil(min) + 1)) + min;
  }
  return randomInt
}

function getRandomFromArray(arr) {
  let index = getRandomInt(0, arr.length-1)
  return arr[index]
}

function getRandomArrFromArray(arr) {
  let usedIndexes = []
  let newArrLength = getRandomInt(1, arr.length-1)
  let newArr = []
  for (let i = 0; i < newArrLength; i++) {
    let index = getRandomInt(0, arr.length-1, usedIndexes)
    usedIndexes.push(index)
    newArr.push(arr[index])
  }
  return newArr
}

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}