require('dotenv').config()
const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const Area = require('../../models/Area')
const Lesson = require('../../models/Lesson')
const Fields = require('../../models/Fields')
const AreaAuth = require('../../models/AreaAuth')
const MasterAuth = require('../../models/MasterAuth')
const generateFakeLesson = require('./generateFakeLesson')
const { demoAreas, demoFields } = require('./demoData.json') 

const resetDemoData = async() => {
  console.time('reset data')
  await mongoose.connect(process.env.MONGO_URI)
  await resetAreas()
  await resetLessons()
  await resetFields()
  await resetAreaAuths()
  await resetMasterAuth()
  console.timeEnd('reset data')
  process.exit(1)
}
resetDemoData()

const resetAreas = async () => {
  await Area.deleteMany({}).exec()
  await Area.insertMany(demoAreas)
}

const resetLessons = async () => {
  await Lesson.deleteMany({}).exec()
  await Lesson.counterResetPromise('index_counter')
  const lessons = []
  let index = 1
  await Promise.all(demoAreas.map(async area => {
    index = 1
    await Promise.all(Array(area.lessonsToCreate).fill().map(async () => {
      lessons.push({
        index,
        areaId: area._id,
        ...generateFakeLesson()
      })
      index++
    }))
  }))
  await Lesson.create(lessons)
}

const resetFields = async () => {
  await Fields.deleteMany({}).exec()
  await Promise.all(demoAreas.map(async area => {
    await new Fields({
      areaId: area._id,
      fields: demoFields
    }).save()
  }))
}

const resetAreaAuths = async () => {
  await AreaAuth.deleteMany({}).exec()
  await Promise.all(demoAreas.map(async area => {
    await new AreaAuth({
      areaId: area._id,
      hashedPassword: await bcrypt.hash('12345', 1)
    }).save()
  }))
}

const resetMasterAuth = async () => {
  await MasterAuth.deleteMany({}).exec()
  await new MasterAuth({
    username: 'admin',
    hashedPassword: await bcrypt.hash('123', 1)
  }).save()
}