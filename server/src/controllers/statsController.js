const Lesson = require('../models/Lesson')
const resolveError = require('../utils/defaultErrorResolver')

exports.getStats = async (req, res) => {

  const { areaId } = req.params
  const { filters } = req.query

  try {
    const stats = await calculateStats({ areaId, ...filters })
    res.status(200).send(stats)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.getFieldStats = async (req, res) => {

  const { areaId, field } = req.params
  const { filters } = req.query

  try {
    const fieldValues = await Lesson.distinct(field, { areaId, ...filters })
    const fieldStats = await Promise.all(fieldValues.map(async value => ({
      value,
      ...await calculateStats({ areaId, [field]: value, ...filters })
    })))
    res.status(200).send(sortFieldStats(fieldStats))
  } catch (err) {
    resolveError(res, err)
  }
}


//-- utils --//

async function calculateStats(filters) {
  const totalCount = await Lesson.find({ ...filters }).count()
  const greenCount = await Lesson.find({ ...filters, status: 'בוצע' }).count()
  const yellowCount = await Lesson.find({ ...filters, status: 'בביצוע' }).count()
  const redCount = await Lesson.find({ ...filters, status: 'לא בוצע' }).count()  

  return {
    counts: {
      green: greenCount,
      yellow: yellowCount,
      red: redCount,
    },
    percentages: {
      green: (greenCount/totalCount)*100 || 0,
      yellow: (yellowCount/totalCount)*100 || 0,
      red: (redCount/totalCount)*100 || 0,
    }
  }
}

function sortFieldStats(stats) {
  return stats.sort((a, b) => {
    if (a.percentages.green < b.percentages.green) return -1
    if (a.percentages.green > b.percentages.green) return 1
    return 0
  })
}