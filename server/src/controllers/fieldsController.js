const Fields = require('../models/Fields')
const resolveError = require('../utils/defaultErrorResolver')

exports.getFields = async (req, res) => {
  
  const { areaId } = req.params

  try {
    const { fields } = await Fields.findOne({ areaId }).select('fields')
    res.status(200).send(fields)
  } catch (err) {
    resolveError(res, err)
  }
}

exports.setFields = async (req, res) => {

  const { areaId } = req.params
  const { body } = req

  try {
    const { fields } = await Fields.findOneAndUpdate({ areaId }, { fields: body }, {new:true,upsert:true}).select('fields')
    res.status(200).send(fields)
  } catch (err) {
    resolveError(res, err)
  }
}