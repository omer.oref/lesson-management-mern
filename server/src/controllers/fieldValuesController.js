const Lesson = require('../models/Lesson')
const resolveError = require('../utils/defaultErrorResolver')

exports.getFieldValues = async (req, res) => {
  
  const { areaId, fields } = req.params
  const { filters } = req.query

  try {
    let fieldValues
    const fieldsArr = fields.split(',')
    if (fieldsArr.length == 1) {
      fieldValues = await Lesson.distinct(fieldsArr[0], { areaId, ...filters })
    } else {
      fieldValues = await Promise.all(fieldsArr.map(async field => (
        await Lesson.distinct(field, { areaId, ...filters })
      )))
    }
    res.status(200).send(fieldValues)
  } catch (err) {
    resolveError(res, err)
  }
}
