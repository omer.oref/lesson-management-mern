const {Types:{ ObjectId }} = require('mongoose')
const Area = require('../models/Area')
const resolveError = require('../utils/defaultErrorResolver')

module.exports = async (req, res, next) => {

  const { areaId } = req.params

  try {
    if (!ObjectId.isValid(areaId) || !await Area.exists({ _id: areaId })) {
      throw {
        code: 404,
        errorMessage: 'Area not found'
      }
    }
    next()
  } catch (err) {
    resolveError(res, err)
  }
}