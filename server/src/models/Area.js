const mongoose = require('mongoose')

const areaSchema = new mongoose.Schema({
  name: { 
    type: String, 
    required: true 
  },
  hue: { 
    type: Number, 
    required: true, 
    default: 220 
  },
  lessonsCreated: {
    type: Number,
    required: true,
    min: 0,
    default: 0,
  }
}, { timestamps: true, toObject: {virtuals:true}, toJSON: {virtuals:true} })

// virtuals
areaSchema.virtual('lessonsAvailable', {
  ref: 'Lesson',
  localField: '_id',
  foreignField: 'areaId',
  justOne: false,
  count: true
})

// hooks
areaSchema.pre('find', function(next) {
  this.populate('lessonsAvailable'); next()
})
areaSchema.pre('findOne', function(next) {
  this.populate('lessonsAvailable'); next()
})
areaSchema.pre('save', function(next) {
  this.populate('lessonsAvailable'); next()
})

module.exports = mongoose.model('Area', areaSchema)