const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const areaAuthSchema = new mongoose.Schema({
  areaId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    unique: true
  },
  hashedPassword: {
    type: String,
    required: true
  }
})

areaAuthSchema.statics.register = async function(areaId, password) {
  if (await this.findOne({ areaId })) {
    throw {
      code: 400,
      errorMessage: 'Area already registered'
    }
  }
  const hashedPassword = await bcrypt.hash(password, 10)
  await this.create({ areaId, hashedPassword })
  return this.createToken(areaId)
}

areaAuthSchema.statics.authenticate = async function(areaId, password) {
  const areaAuth = await this.findOne({ areaId })
  if (!areaAuth || !await bcrypt.compare(password, areaAuth.hashedPassword)) {
    throw {
      code: 401,
      errorMessage: 'Authentication failed'
    }
  }
  else return this.createToken(areaId)
}

areaAuthSchema.statics.createToken = areaId => {
  return jwt.sign({
    areaId,
    role: 'AREA_ADMIN'
  }, process.env.SECRET, {expiresIn: '7d'})
}

module.exports = mongoose.model('AreaAuth', areaAuthSchema)