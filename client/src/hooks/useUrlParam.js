import { useState, useEffect } from "react"
import { useSearchParams } from 'react-router-dom'

export default function useUrlParam(paramName, defaultValue) {

  const [searchParams, setSearchParams] = useSearchParams()
  const [param, setParamState] = useState(defaultValue)

  useEffect(() => {
    const paramParsed = 
      isJson(searchParams.get(paramName)) 
      ? JSON.parse(searchParams.get(paramName))
      : searchParams.get(paramName)
    setParamState(paramParsed || defaultValue)
  }, [searchParams])

  const setParam = value => {
    if (value) {
      if (typeof value == 'object') {
        value = JSON.stringify(value)
      }
      setSearchParams({ ...Object.fromEntries([...searchParams]), [paramName]: value })
    } else {
      const {[paramName]:_, ...restParams} = Object.fromEntries([...searchParams])
      setSearchParams(restParams)
    }
  }

  return [param, setParam]
}

const isJson = (item) => {
  item = typeof item != "string"
      ? JSON.stringify(item)
      : item;
  try {
      item = JSON.parse(item);
  } catch (e) {
      return false;
  }

  if (typeof item == "object" && item != null) {
      return true;
  }

  return false;
}

// const isFalsy = value => {
//   if (!value) return true
//   if (Array.isArray(value)) return !value.length
//   if (typeof value == 'object') return !Object.values(value).length
//   return false
// }