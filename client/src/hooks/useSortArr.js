import { useState, useEffect } from "react"

export default function useSortArr(arr, param) {

  const [sortedArr, setSortedArr] = useState([])

  useEffect(() => {
    if (arr) {
      setSortedArr(sortArr(arr, param))
    }
  },[arr])

  const sortArr = (arr, param) => {
    let newArr = [...arr]
    newArr.sort((a, b) => {
      if (a[param] < b[param]) return -1
      if (a[param] > b[param]) return 1
      return 0
    })
    return newArr
  }

  return sortedArr
}