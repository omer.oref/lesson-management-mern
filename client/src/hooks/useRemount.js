import { useState, useCallback } from "react"

export default function useRemount () {
  const [componentKey, setComponentKey] = useState(0)

  const remount = useCallback(() => {
    setComponentKey(Date.now())
  }, [setComponentKey])
  
  return {
    componentKey,
    remount
  }
}