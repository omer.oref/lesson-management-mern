import { Routes, Route } from 'react-router-dom'

// components
import Header from '@/components/header/Header'
import AreaPickerPage from '@/pages/area_picker_page'
import PagesRouter from '@/pages/PagesRouter'

export default function AppRouter() {
  return (
    <Routes>
      <Route path="/" element={
        <AreaPickerPage />
      }/>
      <Route path="/:areaId/*" element={<>
        <Header />
        <PagesRouter />
      </>}/>
    </Routes>
  )
}