export const DEFAULT_HUE = 220

export const FIELDS_TYPES_NAMES = { 
  event: 'אירוע',
  select: 'בחירה',
  textbox: 'טקסט חופשי',
  date: 'תאריך',
  status: 'סטטוס',
  multiselect: 'בחירה מרובה'
}

export const DEFAULT_FIELDS = [
  {
    id: 1,
    name: 'אירוע',
    dataKey: 'drill',
    type: 'event',
    required: true,
    index: 0
  },
  {
    id: 2,
    name: 'נושא',
    dataKey: 'subject',
    type: 'select',
    required: true,
    index: 1
  },
  {
    id: 3,
    name: 'משימה',
    dataKey: 'mission',
    type: 'textbox',
    required: true,
    index: 2
  },
  {
    id: 4,
    name: 'תג"ב',
    dataKey: 'dateToFinish',
    type: 'date',
    required: true,
    index: 3
  },
  {
    id: 5,
    name: 'סטטוס',
    dataKey: 'status',
    type: 'status',
    required: true,
    index: 4
  },
  {
    id: 6,
    name: 'אחריות',
    dataKey: 'responsibility',
    type: 'multiselect',
    required: true,
    index: 5
  }
]