export const getLastDrill = drills => {
  if (!Array.isArray(drills)) return undefined
  let lastDrill = {date:new Date(0)}
  drills.forEach(drill => {
    if (Date.parse(drill.date) > Date.parse(lastDrill.date)) {
      lastDrill = drill
    }
  })
  return lastDrill
}

export const getLastYear = years => {
  if (!Array.isArray(years)) return undefined
  let lastYear = 0
  years.forEach(year => {
    if (Number(year) > Number(lastYear)) {
      lastYear = year
    }
  })
  return lastYear
}

export const flattenValue = value => {
  if (typeof value == 'object') {
    if (Array.isArray(value)) {
      return value.join(', ')
    }
    else {
      return value.name
    }
  }
  return value
}