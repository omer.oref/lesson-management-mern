import { DEFAULT_HUE } from "./defaults"

export default function setAppHue(hue=DEFAULT_HUE) {
  document.querySelector('.App').style.setProperty('--main-hue', (hue??DEFAULT_HUE)+'deg');
}
