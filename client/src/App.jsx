import { useContext } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Toaster } from 'react-hot-toast'
import { ReactQueryDevtools } from 'react-query/devtools'

// utils
import { ReactQueryProvider } from '@/lib/react-query/config'

// containers
import AppErrorBoundary from '@/containers/AppErrorBoundary'
import MuiThemeProvider from '@/containers/MuiThemeProvider'

// context
import { ThemeContext } from '@/context/ThemeProvider'

// components
import AppRouter from './AppRouter'
import GlobalFetchingIndicator from '@/lib/react-query/GlobalFetchingIndicator'

// styles
import './App.scss'

export default function App() {

  const { theme } = useContext(ThemeContext)

  return (
    <div className={`App ${theme}-mode`}>
      <BrowserRouter>
        <AppErrorBoundary>
          <MuiThemeProvider>
            <ReactQueryProvider>
              <AppRouter />
              <div className="modals" />
              <GlobalFetchingIndicator />
              <ReactQueryDevtools position='bottom-right' initialIsOpen={false} />
            </ReactQueryProvider>
          </MuiThemeProvider>
        </AppErrorBoundary>
      </BrowserRouter>
      <Toaster 
        position="top-right" 
        toastOptions={{className:"toast",style:{pointerEvents:'none'}}} 
      />
    </div>
  )
}