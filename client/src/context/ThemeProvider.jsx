import { createContext } from 'react'

// custom hooks
import useLocalStorage from '@/hooks/useLocalStorage'

// context
export const ThemeContext = createContext()

// provider
export function ThemeProvider({ children }) {

  const [theme, setTheme] = useLocalStorage('theme', 'light')

  return (
    <ThemeContext.Provider value={{ theme, setTheme }}>
      {children}
    </ThemeContext.Provider>
  )
}
