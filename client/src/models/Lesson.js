import { array, object, string } from "yup"

export const lessonSchema = object({
  drill: object({
    name: string().required(), 
    date: string().required()
  }).required(),
  subject: string().required(),
  mission: string().required(),
  dateToFinish: object({
    name: string().required(),
    year: string().required()
  }).required(),
  status: string().required().oneOf(
    ['בוצע', 'בביצוע', 'לא בוצע']
  ),
  responsibility: array(string().required()).required()
})
