import { array, boolean, number, object, string } from "yup"

export const fieldsSchema = array(object({
  id: string().required(),
  name: string().required(),
  dataKey: string().required(),
  type: string().required().oneOf(
    ['event', 'select' ,'textbox' ,'date' ,'status' ,'multiselect']
  ),
  index: number().required(),
  required: boolean().required().default(false),
}))