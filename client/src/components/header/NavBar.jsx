// router
import { NavLink, useLocation } from "react-router-dom"

// components
import ImgLoader from '@/components/general/ImgLoader'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'

// styles
import "./NavBar.scss"

// images
import Home from '@/assets/home.svg'
import Table from '@/assets/table.svg'
import Stats from '@/assets/pie.svg'
import Me from '@/assets/person.svg'
import Filter from '@/assets/filter.svg'

// nav links
const navLinks = [
  { id: 1, text: "עמוד הבית", to: " ", img: Home },
  { id: 2, text: "טבלת לקחים", to: "table", img: Table },
  { id: 3, text: "תמונת מצב לקחים", to: "stats", img: Stats },
  { id: 4, text: "הלקחים שלי", to: "me", img: Me },
  { id: 5, text: "מיון אירוע/שנה", to: "filter", img: Filter }
]

export default function NavBar() {

  const { pathname } = useLocation()
  const currPath = pathname.split('/')[2]||" "
  const noMatch = navLinks.every(btn => btn.to != currPath)
  const value = noMatch ? 'noMatch' : currPath

  return (
    <Tabs
      value={value}
      scrollButtons="auto"
      allowScrollButtonsMobile
      className="nav"
      variant="scrollable"
      TabIndicatorProps={noMatch?{
        style: { display: 'none' }
      }:{}}
    >
      <Tab value="noMatch" className="hidden" />
      {navLinks.map(link => (
        <Tab 
          disableRipple
          key={link.id}
          value={link.to}
          LinkComponent={NavLink}
          to={link.to} 
          icon={<ImgLoader src={link.img} loaderSize="2em" />}
          tool-bottom={link.text}
          className="nav-link"
        />
      ))}
    </Tabs>
  )
}
