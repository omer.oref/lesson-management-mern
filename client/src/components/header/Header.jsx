// router
import { Link } from 'react-router-dom'

// queries
import { useArea } from '@/lib/react-query/queries'

// custom hooks
import useAuth from '@/hooks/useAuth'

// containers
import QueryLoader from '@/containers/QueryLoader'
import { Fade } from '@/containers/Fade'

// components
import NavBar from './NavBar'
import Areatitle from '@/components/general/AreaTitle'
import EditAreaBtn from '@/components/admin/EditAreaBtn'
import BtnPrimary from '@/components/general/BtnPrimary'
import ThemeSwitch from '@/components/general/ThemeSwitch'
import ElementLoader from '@/components/general/ElementLoader'
import Skeleton from '@mui/material/Skeleton'

// styles
import './Header.scss'

// images
import BackArrow from '@/assets/arrow_right2.svg'

export default function Header() {

  const query = useArea()
  const { isAreaAdmin } = useAuth()

  return (
    <Fade>
      <header>
        <div className="right-section">
          <Link to="/" className="home-btn">
            <img src={BackArrow} className="back-arrow"/>
            <QueryLoader 
              query={query} 
              LoadingIndicator={
                isLoading => (
                  <span onClick={e => e.stopPropagation()}>
                    <ElementLoader element={document.querySelector('.App')} isLoading={isLoading} hideElementOnLoad />
                    {isLoading && <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" style={{ margin: '0 .5em' }} />}    
                  </span>
                )
              }
              onError={err => {throw err}}
            >
              <Areatitle area={query.data} />
            </QueryLoader>
          </Link>
          <div>
            {isAreaAdmin && <Fade>
              <EditAreaBtn>
                <BtnPrimary>
                  עריכת סביבה
                </BtnPrimary>
              </EditAreaBtn>
            </Fade>}
          </div>
        </div>
        <NavBar />
        <div className="left-section">
          <ThemeSwitch/>
        </div>
      </header>
    </Fade>
  )
}
