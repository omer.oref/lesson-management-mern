import Btn from '@/components/general/Btn'

export default function FormButtons({
  variant,
  submitText,
  cancelText,
  noResetBtn,
  disabled,
  isLoading,
  onSubmit,
  onReset
}) {
  return (
    <div className="form-buttons">
      {!noResetBtn && (
        <button type="reset" className="close-btn" onClick={onReset}>
          {cancelText || 'ביטול'}
        </button>
      )}
      <Btn
        type="submit"
        style={{ margin: '0 .5em' }}
        disabled={disabled}
        isLoading={isLoading}
        onClick={onSubmit}
        color={variant || ''}
      >
        {submitText || 'הוסף/י'}
      </Btn>
    </div>
  )
}
