import { forwardRef } from "react"
import inputHOC from "./InputHOC"

export default inputHOC(forwardRef(({
  value,
  onChange,
  ...restProps
}, ref) => {

  const handleChange = e => {
    onChange(e, {
      name: e.target.value,
      year: new Date(e.target.value).getFullYear()
    })
  }

  return (
    <input 
      className="date-input"
      type="date"
      ref={ref}
      value={value.name || ''}
      onChange={handleChange}
      {...restProps}
    />
  )
}))