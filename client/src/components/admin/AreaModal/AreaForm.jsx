// components
import HuePicker from '@/components/general/HuePicker'
import Tooltip from '@/components/general/Tooltip'
import Input from '@/components/general/Input'
import StyledContainer from '@/components/general/StyledContainer'

// styles
import './AreaForm.scss'

const PreviewColor = () => (
  <div className="preview-color">
    <div className="preview-header"/>
  </div>
)

export default function AreaForm({ 
  areaData,
  setAreaData
}) {
  return (
    <div 
      className="area-form" 
      style={{ '--main-hs': `${areaData.hue}deg, var(--main-satur)`}}
    >
      <StyledContainer 
        className="top"
      >
        <div className="inputs">
          <div className="color-input" tabIndex="0">
            <Tooltip>
              <div className="color-picker">
                <PreviewColor />
                <HuePicker
                  hue={areaData.hue}
                  onChange={hue => setAreaData(prev => ({...prev, hue}))}
                />
              </div>
            </Tooltip>
          </div>
          <Input 
            type="text" 
            state={[areaData.name, name => setAreaData(prev => ({...prev, name}))]} 
            required
            placeholder="שם הסביבה"
          />
        </div>
      </StyledContainer>
    </div>
  )
}