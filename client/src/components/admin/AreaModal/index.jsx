import { useEffect, useState } from 'react'

// utils
import { DEFAULT_HUE, DEFAULT_FIELDS } from '@/utils/defaults'

// components
import AreaForm from "./AreaForm"
import FieldsForm from './FieldsForm'
import PinForm from './PinForm'
import Modal from '@/components/general/Modal'
import Btn from '@/components/general/Btn'
import { FormButtons } from '@/components/form'

export default function AreaModal({ 
  onSubmit, 
  defaultData, 
  modalState, 
  isLoading,
  withPassword,
  children 
}) {
  const [showModal, setShowModal] = modalState

  const defaultAreaName = defaultData?.area?.name ?? ''
  const defaultAreaHue = defaultData?.area?.hue ?? DEFAULT_HUE
  const defaultfields = defaultData?.fields ?? DEFAULT_FIELDS
  
  const [areaData, setAreaData] = useState({ name: defaultAreaName, hue: defaultAreaHue })
  const [fields, setFields] = useState(defaultfields)
  const [password, setPassword] = useState('')

  useEffect(() => {
    setAreaData({ name: defaultAreaName, hue: defaultAreaHue })
    setFields(defaultfields)
  }, [defaultAreaName, defaultAreaHue, defaultfields])

  const handleSubmit = () => {
    if (areaData.name && fields) {
      onSubmit(areaData, fields, password)
    }
  }

  const handleReset = () => {
    setShowModal(false)
    setAreaData({ name: defaultAreaName, hue: defaultAreaHue })
    setFields(defaultfields)
    setPassword('')
  }

  return (
    <div>
      <div 
        className="area-modal" 
        onClick={() => setShowModal(true)}
      >
        {children}
      </div>
      <Modal 
        showModal={showModal=='true'} 
        setShowModal={setShowModal}
      >
        <AreaForm {...{areaData, setAreaData}} />
        <FieldsForm {...{fields, setFields}} />
        {withPassword && (
          <PinForm {...{password, setPassword}} />
        )}
        <FormButtons
          onSubmit={handleSubmit}
          onReset={handleReset}
          submitText={!defaultData ? 'הוסף/י סביבה' : 'שמירת שינויים'}
          disabled={!areaData.name || (withPassword && password.length != 5)}
          isLoading={isLoading}
        />
      </Modal>
    </div>
  )
}