// components
import FieldsFormAddForm from './FieldsFormAddForm'
import ImgLoader from '@/components/general/ImgLoader'
import Tooltip from '@/components/general/Tooltip'

// images
import PlusImg from '@/assets/plus.svg'

export default function FieldsFormAdd({ setFields, focusOut }) {

  const addField = fieldData => {
    setFields(fields => [...fields, {
      id: Date.now(),
      dataKey: fieldData.name,
      index: fields?.length || 0,
      required: false,
      ...fieldData
    }])
    focusOut()
  }

  return (
    <>
      <span 
        tabIndex="0"
        className="img-button"
      >
        <ImgLoader loaderSize="1.5em" className="img-black" src={PlusImg} />
        <Tooltip>
          <FieldsFormAddForm onSubmit={addField} />
        </Tooltip>
      </span>
    </>
  )
}