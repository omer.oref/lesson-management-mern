import { useRef } from 'react'

// components
import Prompt from '@/components/general/Prompt'
import ErrorAlert from '@/components/general/ErrorAlert'

export default function DeleteAreaBtn({ mutation, areaId, children }) {

  const promptRef = useRef()

  const handleClick = async () => {
    if (mutation.isLoading) return
    await promptRef.current.prompt()
    mutation.mutate(areaId)
  }

  return (
    <div onClick={handleClick}>
      {children}
      <Prompt ref={promptRef} submitText="מחיקה" variant="danger">
        <ErrorAlert title="למחוק סביבה?" message="פעולה זו לא ניתנת לשינוי!" />
      </Prompt>
    </div>
  )
}