import { useState, useEffect } from 'react'

// custom hooks
import useRemount from '@/hooks/useRemount'

// components
import { 
  Form, 
  EventInput, 
  SelectInput, 
  BoxInput, 
  DateInput, 
  MultiselectInput,
  FormButtons
} from '@/components/form'

export default function LessonForm({ onSubmit, defaultData, setShowModal, formFields }) {

  const [formData, setFormData] = useState(defaultData || {})
  const { componentKey, remount } = useRemount()

  useEffect(() => {
    setFormData(defaultData || {})
  }, [defaultData])
  
  const handleSubmit = () => {
    onSubmit(formData)
  }

  const handleReset = () => {
    setFormData(defaultData || {})
    setShowModal(false)
    remount()
  }

  return (
    <Form 
      formState={[formData, setFormData]} 
      onSubmit={handleSubmit}
      onReset={handleReset}
      autoComplete="off"
      disableEnterSubmit
      key={componentKey}
    >
      {formFields.map(field => {
        switch (field.type) {
          case 'event': return (
            <EventInput
              key={field.dataKey}
              dataKey={field.dataKey}
              label={field.name} 
              options={field.options}
              required={field.required}
              toggleable
              noCreatable
            />
          )
          case 'select': return (
            <SelectInput
              key={field.dataKey}
              dataKey={field.dataKey}
              label={field.name} 
              options={field.options}
              required={field.required}
            />
          )
          case 'textbox': return (
            <BoxInput 
              key={field.dataKey}
              dataKey={field.dataKey}
              label={field.name} 
              required={field.required}
            />
          )
          case 'date': return (
            <DateInput
              key={field.dataKey}
              dataKey={field.dataKey}
              label={field.name} 
              required={field.required}          
            />
          )
          case 'status': return (
            <SelectInput
              key={field.dataKey}
              dataKey={field.dataKey}
              label={field.name} 
              options={['בוצע', 'בביצוע', 'לא בוצע']}
              required={field.required}
              noCreatable
              noSearchable
            />
          )
          case 'multiselect': return (
            <MultiselectInput
              key={field.dataKey}
              dataKey={field.dataKey}
              label={field.name} 
              options={field.options}
              required={field.required}
            />
          )
        }
      })}
      <FormButtons submitText={!defaultData ? 'הוסף/י לקח' : 'שמירת שינויים'} />
    </Form>
  )
}