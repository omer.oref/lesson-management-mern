// components
import Modal from '@/components/general/Modal'
import LessonFormController from './LessonFormController'
import ElementLoader from '@/components/general/ElementLoader'

export default function LessonsModal({
  onSubmit, 
  defaultData, 
  modalState,
  isLoading,
  children 
}) {

  const [showModal, setShowModal] = modalState
  const handleSubmit = formData => {
    onSubmit(formData)
  }

  return (
    <>
      <div
        className="lesson-modal"
        onClick={() => setShowModal(true)}
      >
        {children}
      </div>
      <Modal 
        className="lesson-form-modal-window" 
        showModal={showModal=='true'} 
        setShowModal={setShowModal}
      >
        <LessonFormController 
          onSubmit={handleSubmit} 
          defaultData={defaultData}
          setShowModal={setShowModal} 
        />
      </Modal>
      <ElementLoader
        isLoading={isLoading} 
        element={document.querySelector('.lesson-form-modal-window .btn[type=submit]')}
      />
    </>
  )
}