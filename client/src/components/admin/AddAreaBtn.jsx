// mutations
import { useAddArea } from '@/lib/react-query/mutations'

// hooks
import useUrlParam from '@/hooks/useUrlParam'
import useRemount from '@/hooks/useRemount'

// components
import AreaBtn from './AreaModal'

export default function AddAreaBtn({ children }) {

  const [showModal, setShowModal] = useUrlParam('add_area', 'false')
  const { componentKey, remount } = useRemount()
  const { mutate: addArea, isLoading } = useAddArea()
  
  const handleSubmit = (areaData, fields, password) => {
    if (isLoading) return
    addArea({ areaData, fields, password }, {
      onSuccess: () => {
        setShowModal(false)
        remount()
      }
    })
  }

  return (
    <AreaBtn 
      onSubmit={handleSubmit} 
      modalState={[showModal, setShowModal]}
      isLoading={isLoading}
      key={componentKey}
      withPassword
    >
      {children}
    </AreaBtn>
  )
}