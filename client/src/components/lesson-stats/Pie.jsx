import React from 'react'
import { PieChart, Pie, Cell} from 'recharts'
import { isMobile } from 'react-device-detect'

// queries
import { useStats } from '@/lib/react-query/queries'

// containers
import { FadeDown } from '@/containers/Fade'
import QueryLoader from '@/containers/QueryLoader'

// components
import PieSkeleton from './components/PieSkeleton'
import ErrorAlert from '@/components/general/ErrorAlert'
import EmptyIndicator from '../general/EmptyIndicator'

// styles
import './Pie.scss'

export default function StatsPie({ title, filters }) {

  const statsQuery = useStats({ filters }, {
    select: data => {
      const { green, yellow, red } = data.percentages
      return [
        {value: green, name: 'בוצע', color: 'var(--green)'}, 
        {value: yellow, name: 'בביצוע', color: 'var(--yellow)'}, 
        {value: red, name: 'לא בוצע', color: 'var(--red)'}
      ]
    }
  })
  const hasData = statsQuery.data?.some(x => x.value)

  return (
    <QueryLoader 
      query={statsQuery}
      LoadingIndicator={<PieSkeleton />}
      ErrorIndicator={error => <ErrorAlert message={error?.message} />}
    >
      {statsQuery.data && (
        <div className="pie-con">
          <h3 className="pie-title">{title}</h3>
          {hasData ? (
            <RechartsPie segments={statsQuery.data} />
          ) : (
            <EmptyIndicator style={{ margin: '2em 0'}} text="" />
          )}
          <FadeDown>
            <dl className="pie-info">
              <dd style={{ '--color': 'var(--green)'}}>{statsQuery.data[0].value.toFixed(1)}%</dd>
              <dt>בוצע</dt>

              <dd style={{ '--color': 'var(--yellow)'}}>{statsQuery.data[1].value.toFixed(1)}%</dd>
              <dt>בביצוע</dt>

              <dd style={{ '--color': 'var(--red)'}}>{statsQuery.data[2].value.toFixed(1)}%</dd>
              <dt>לא בוצע</dt>
            </dl>
          </FadeDown>
        </div>
      )}
    </QueryLoader>
  )
}

const RechartsPie = React.memo(({ segments }) => {
  return (
    <PieChart width={200} height={220}>
      <Pie
        isAnimationActive={!isMobile}
        animationDuration={750}
        animationBegin={0}
        stroke="none"
        data={segments}
        cx="50%"
        cy="50%"
        labelLine={false}
        outerRadius={100}
        innerRadius={50}
        dataKey="value"
      >
        {segments?.map((entry, index) => (
          <Cell key={`cell-${index}`} fill={entry.color} />
        ))}
      </Pie>
    </PieChart>
  )
}, (prevProps, nextProps) => JSON.stringify(prevProps.segments) == JSON.stringify(nextProps.segments))