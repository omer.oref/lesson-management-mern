import Skeleton from '@mui/material/Skeleton'

export default function PieSkeleton() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <Skeleton animation="pulse" variant="rectangular" height={40} width={100} style={{ margin: '1em 0' }} />
      <Skeleton animation="pulse" variant="circular" height={220} width={220} />
      <Skeleton animation="pulse" variant="rectangular" height={40} width={250} style={{ margin: '1em 0' }} />
    </div>
  )
}