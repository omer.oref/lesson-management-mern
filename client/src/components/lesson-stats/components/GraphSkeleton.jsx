import Skeleton from '@mui/material/Skeleton'

export default function GraphSkeleton() {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'flex-end' }}>
        <Skeleton animation="pulse" variant="rectangular" height={120} width={80} style={{ margin: '0 10px' }} />
        <Skeleton animation="pulse" variant="rectangular" height={150} width={80} style={{ margin: '0 10px' }} />
        <Skeleton animation="pulse" variant="rectangular" height={200} width={80} style={{ margin: '0 10px' }} />
        <Skeleton animation="pulse" variant="rectangular" height={350} width={40} style={{ margin: '0 10px' }} />
      </div>
      <Skeleton animation="pulse" variant="rectangular" height={40} width={350} style={{ margin: '1em 0' }} />
    </div>
  )
}