// utils
import { getColoredHs } from '../colors'
import { flattenValue } from '@/utils/lessonsUtils'

const keysToColor = ['drill', 'status']

const getColoredProps = key => ({
  className: 'colored',
  style: {'--main-hs': getColoredHs(key)}
})

export default function TableRow({ row, fields, clickable, onClick, onKeyPress }) {
  const { index } = row
  return (
    <tr 
      className={clickable ? "clickable" : "not-clickable"}
      tabIndex={clickable ? '0' : null}
      onClick={() => onClick(index)}
      onKeyPress={e => onKeyPress(e, index)}
    >
      <td>{index}</td>
      {Object.values(fields).map(field => (
        <td 
          key={field.dataKey}
          {...keysToColor.includes(field.dataKey) && 
            getColoredProps(flattenValue(row[field.dataKey]))
          }
        >
          {flattenValue(row[field.dataKey]) ?? '-'}
        </td>
      ))}
    </tr>
  )
}