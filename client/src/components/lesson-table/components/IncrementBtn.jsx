// components
import ElementLoader from '@/components/general/ElementLoader'

// images
import LoadImg from '@/assets/load.svg'

// styles
import './IncrementBtn.scss'

export default function IncrementBtn({ onClick, slicedLength, fullLength, isFull, isLoading }) {

  const getSliceInfo = () => {
    return `מראה ${numberWithCommas(slicedLength)}/${numberWithCommas(fullLength)} לקחים`
  }

  return <>
    <div style={{ paddingBottom: '.5em'}}>
      <div className="slice-count">{getSliceInfo()}</div>
      {!isFull && (
        <button 
          onClick={onClick} 
          className="increment-btn"
        >
          <span>טען עוד</span>
          <img src={LoadImg} />
        </button>
      )}
    </div>
    <ElementLoader 
      element={document.querySelector('.increment-btn')}
      isLoading={isLoading}
    />
  </>
}

const numberWithCommas = num => {
  return num?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}