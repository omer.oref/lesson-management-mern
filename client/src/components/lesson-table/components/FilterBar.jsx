import { useEffect, useRef, useState } from 'react'

// queries
import { useFields, useFieldValues } from '@/lib/react-query/queries'

// utils
import { flattenValue } from '@/utils/lessonsUtils'

// components
import Select from '@/components/general/Select'
import ImgLoader from '@/components/general/ImgLoader'

// images
import RedoImg from '@/assets/redo.svg'

// styles
import './FilterBar.scss'

export default function FilterBar({ filterParams, queryObj, setQueryObj }) {

  const formRef = useRef(null)
  const [filterOptions, setFilterOptions] = useState(
    filterParams.reduce(
      (obj, param) => ({...obj, [param]: []}),
      {}
    )
  )
  const { data: fields } = useFields()
  const getFieldName = dataKey => 
    fields?.find(field => field.dataKey == dataKey)?.name || ''

  const { data: uniqueValues, isLoading } = useFieldValues(filterParams, { filters: queryObj })

  // update available filter options on data change
  useEffect(() => {
    if (!uniqueValues) return

    const filterOptions = {}
    filterParams.forEach((param, index) => {
      filterOptions[param] = Array.isArray(uniqueValues[index]) 
        ? uniqueValues[index] 
        : uniqueValues
    })
    setFilterOptions(filterOptions)
  }, [filterParams, uniqueValues])

  // update queryObj on select change
  const handleChange = (data, key) => {
    if (data) {
      setQueryObj({
        ...queryObj,
        [key]: data
      })
    } else {
      const {[key]:_, ...rest} = queryObj
      setQueryObj(rest)
    }
  }

  return <>
    <div className="filter-bar" ref={formRef}>
      {Object.keys(filterOptions).map(key => (
        <div 
          key={key} 
          className={queryObj[key] ? 'select filled' : 'select'} 
          {...queryObj[key] && {onClick() {handleChange(null,key)}}}
        >
          <Select 
            isLoading={!queryObj[key] && isLoading}
            isDisabled={isLoading && !queryObj[key]}
            name={key}
            value={queryObj[key] || ''}
            onChange={data => handleChange(data,key)}
            options={filterOptions[key].map(option => flattenValue(option))}
            placeholder={`בחר ${getFieldName(key)}`}
            noCreatable
            menuPortalTarget={document.querySelector('.App')}
            {...(queryObj[key] || isLoading) && {components:{
              input: () => null,
              Menu: () => null,
              MenuList: () => null,
              DropdownIndicator: () => null,
              IndicatorSeparator: () => null
            },noSearchable:true,openMenuOnClick:false}}
          >
          </Select >
        </div>
      ))}
      {filterParams?.length > 1 && (
        <button className="reset-btn" onClick={() => setQueryObj(null)} tool-top="אתחל סינונים" >
          <ImgLoader src={RedoImg} loaderSize="1.5em" className="img-gray" />
        </button>
      )}
    </div>
  </>
}
