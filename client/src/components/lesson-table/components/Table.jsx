import { useState, useEffect } from 'react'
import { isMobile } from 'react-device-detect'

// containers
import { Fade } from '@/containers/Fade'

// custom hooks
import useWindowDimensions from '@/hooks/useWindowDimentions'

// components
import TableHead from './TableHead'
import TableBody from './TableBody'
import List from './List'
import Switch from '@/components/general/Switch'
import ImgLoader from '@/components/general/ImgLoader'

// images
import TableIcon from '@/assets/tableIcon.svg'
import ListIcon from '@/assets/listIcon.svg'

// styles
import './Table.scss'

export default function Table({ tableArr, clickable, listMode=false }) {

  const listModeState = useState(isMobile ? true : listMode)
  const [isListMode, setListMode] = listModeState

  const { width } = useWindowDimensions()

  useEffect(() => {
    width <= 1100 && setListMode(true)
  },[width])

  return (
    <>
    <div className="table-switch">
      <Switch 
        state={listModeState}
        label="החלף בין טבלה לרשימה"
        offLabel={<ImgLoader loaderSize="1.5em" src={TableIcon} className="img-black" style={{ height: '1.5em'}}/>}
        onLabel={<ImgLoader loaderSize="1.5em" src={ListIcon} className="img-black" style={{ height: '1.5em'}}/>}
      />
    </div>
    {!isListMode && (
      <Fade style={{ width: '100%', padding: '0 1em' }}>
        <table className="table">
          <TableHead />
          <TableBody
            tableArr={tableArr}
            clickable={clickable}
          />
        </table>
      </Fade>
    )}
    {isListMode && (
      <Fade style={{ width: '100%', padding: '0 1em' }}>
        <div className="table-list">
          <List
            listArr={tableArr}
            clickable={clickable}
          />
        </div>
      </Fade>
    )}
    </>
  )
}