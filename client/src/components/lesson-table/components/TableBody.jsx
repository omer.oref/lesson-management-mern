// router
import { useNavigate, useLocation } from 'react-router-dom'

// queries
import { useFields } from '@/lib/react-query/queries'

// components
import TableRow from './TableRow'

export default function TableBody({ tableArr, clickable }) {

  const {data: fields} = useFields()
  const { pathname } = useLocation()
  const navigate = useNavigate()

  const handleClick = index => {
    if (!clickable) return
    const pathArr = pathname.split('/')
    pathArr[pathArr.length-1] = `lessons/${index}`
    const pathString = pathArr.join('/')
    navigate(pathString)  
  }
  
  const handleKeyPress = (e, index) => {
    if (e.key == 'Enter' || e.charCode == 32)
      handleClick(index)
  }

  return fields ? (
    <tbody>
      {tableArr.map(row =>
        <TableRow 
          key={row.index} 
          row={row} 
          fields={fields}
          clickable={clickable} 
          onClick={handleClick}
          onKeyPress={handleKeyPress}
        />
      )}
    </tbody>
  ) : null
}