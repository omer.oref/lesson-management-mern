import React from 'react'

// utils
import { getColoredHs } from '../colors'
import { flattenValue } from '@/utils/lessonsUtils'

const keysToColor = ['drill', 'status']

const getColoredProps = key => ({
  className: 'colored',
  style: {'--main-hs': getColoredHs(key)}
})

export default function ListItem({ item, clickable, onClick, onKeyPress, fields, getFieldName }) {
  const { index } = item
  return (
    <li
      className={clickable ? "clickable" : "not-clickable"}
      tabIndex={clickable ? '0' : null}
      onClick={() => onClick(index)}
      onKeyPress={e => onKeyPress(e, index)}
    >
      <h2>סימוכין: {index}</h2>

      <dl>
        {Object.values(fields).map(field =>
          <React.Fragment key={field.dataKey}>
            <dt>{getFieldName(field.dataKey)}</dt>
            <dd 
              {...keysToColor.includes(field.dataKey) && 
                getColoredProps(flattenValue(item[field.dataKey]))
              }
            >
              {flattenValue(item[field.dataKey]) ?? '-'}
            </dd>
          </React.Fragment>
        )}
      </dl>
    </li>
  )
}