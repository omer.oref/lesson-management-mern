// router
import { useNavigate, useLocation } from 'react-router-dom'

// queries
import { useFields } from '@/lib/react-query/queries'

// components
import ListItem from "./ListItem"

// styles
import './List.scss'

export default function List({ listArr, clickable }) {

  const { data: fields, isLoading } = useFields()
  const getFieldName = dataKey => 
    fields?.find(field => field.dataKey == dataKey)?.name || ''

  const { pathname } = useLocation()
  const navigate = useNavigate()

  const handleClick = index => {
    if (!clickable) return
    const pathArr = pathname.split('/')
    pathArr[pathArr.length-1] = `lessons/${index}`
    const pathString = pathArr.join('/')
    navigate(pathString)  
  }
  
  const handleKeyPress = (e, id) => {
    if (e.key == 'Enter' || e.charCode == 32)
      handleClick(id)
  }

  return !isLoading ? (
    <ul>
      {listArr.map(item => 
        <ListItem 
          key={item.index}
          item={item}
          clickable={clickable}
          onClick={handleClick}
          onKeyPress={handleKeyPress}
          fields={fields}
          getFieldName={getFieldName}
        />
      )}
    </ul>
  ) : null
}