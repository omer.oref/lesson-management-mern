// components
import ImgLoader from '@/components/general/ImgLoader'

// images
import PakarLogo from '@/assets/PakarBlack.svg'
import LogoPrimary from '@/assets/logo.svg'
import LogoText from '@/assets/logoText.svg'

// styles
import './SiteLogo.scss'

export default function SiteLogo(props) {
  return (
    <div className="logos" {...props}>
      <ImgLoader className="pakar-logo" src={PakarLogo} loaderSize="12em" skeletonVarient="circular"/>
      <ImgLoader className="logo-primary" src={LogoPrimary} loaderWidth="20em" loaderHeight="8em" />
      <ImgLoader className="logo-text" src={LogoText} loaderWidth="30em" loaderHeight="5em" />
    </div>
  )
}
