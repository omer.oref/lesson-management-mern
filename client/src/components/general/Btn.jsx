import { useRef } from 'react'

// components
import ElementLoader from '@/components/general/ElementLoader'

// styles
import './Btn.scss'

export default function Btn({ children, isLoading, color, ...restProps }) {
  
  const btnRef = useRef()
  
  return <>
    <button
      ref={btnRef}
      {...restProps}
      className={`btn ${color}`}
    >
      {children}
    </button>
    <ElementLoader
      element={btnRef.current}
      isLoading={isLoading}
    />
  </>
}