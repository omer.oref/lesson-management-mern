// styles
import './AreaTitle.scss'

export default function AreaTitle({ area, clickable, showLessonsNum, ...restProps }) {

  const { name, lessonsAvailable, hue } = area || {}
  const Tag = clickable ? 'button' : 'div'

  return (
    <Tag
      {...restProps}
      className={`area-title ${clickable?'clickable':''} ${restProps.className || ''}`}
      style={{'--main-hue': hue+'deg'}} 
    >
      <span className="color"></span>
      <p className="name">{name}</p>
      {showLessonsNum && 
        <span className="lessons-num">{numberWithCommas(lessonsAvailable)} לקחים</span>
      }
    </Tag>
  )
}

const numberWithCommas = x => {
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}