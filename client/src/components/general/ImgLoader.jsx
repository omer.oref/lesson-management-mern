import { useState, useEffect, useRef } from "react"
import styled from "styled-components"

// components
import Skeleton from '@mui/material/Skeleton'

const Img = styled.img`
  ${props => !props.loaded && 
    `position: absolute;`
  }
  ${props => !props.transitionDone && 
    `transition: opacity ${TRANSITION_DURATION_MS}ms, 
    transform ${TRANSITION_DURATION_MS}ms !important;`
  }
  opacity: ${props => props.loaded ? '1' : '0'}
`

const TRANSITION_DURATION_MS = 500

export default function ImgLoader({ 
  loaderSize, 
  loaderHeight, 
  loaderWidth, 
  skeletonVarient, 
  ...props 
}) {

  const imgRef = useRef()
  const [loaded, setLoaded] = useState(false)
  const [transitionDone, setTransitionDone] = useState(false)

  const timeOut = useRef(null)

  useEffect(() => {
    if (loaded) {
      timeOut.current = setTimeout(() => {
        setTransitionDone(true)
      }, TRANSITION_DURATION_MS);
    }
    return () => clearTimeout(timeOut.current)
  }, [loaded])
  
  useEffect(() => {
    if (!imgRef.current.complete) {
      setLoaded(false)
    }
  }, [props.src])

  return <>
    {(loaderSize || loaderHeight || loaderWidth) && !loaded && 
      <Skeleton 
        animation="pulse"
        variant={skeletonVarient || 'rectangular'}
        height={loaderHeight || loaderSize}
        width={loaderWidth || loaderSize}
      />
    }
    <Img 
      ref={imgRef}
      loaded={loaded}
      transitionDone={transitionDone}
      onLoad={() => setLoaded(true)}
      {...props}
    />
  </>
}