import styled from 'styled-components'

// components
import ImgLoader from '@/components/general/ImgLoader'

// images
import ErrorImg from '@/assets/error.svg'

// styles
const ErrorAlertStyled = styled.div`
  text-align: center;
  padding: .5em;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-radius: .5em;
  color: hsla(1deg, 84%, 70%, 100%);
  background-color: hsla(1deg, 84%, 70%, 10%);
  
  .top {
    display: flex;
    align-items: center;

    img {
      width: 1.25em;
      height: 1.25em;
    }
    h4 {
      margin: 0 .2em;
      margin-top: .1em;
    }
  }

  .message {
    margin: .5em;
  }
`

export default function ErrorAlert({ message, title }) {
  return (
    <ErrorAlertStyled>
      <div className="top">
        <ImgLoader src={ErrorImg} />
        <h4>{title || 'שגיאה!'}</h4>
      </div>
      {message && <span className="message">{message}</span>}
    </ErrorAlertStyled>
  )
}