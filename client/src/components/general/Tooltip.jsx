import { useRef, useEffect } from 'react'

// styles
import './Tooltip.scss'

export default function Tooltip({ children, down, ...restProps }) {

  const toolRef = useRef(null)

  useEffect(() => {
    const directionClass = down ? 'tooltip-down' : 'tooltip-up'
    toolRef.current.parentNode.classList += ` tooltip-parent ${directionClass}`
  }, [toolRef, down])

  return (
    <div className="tooltip" ref={toolRef} {...restProps}>
      {children}
    </div>
  )
}