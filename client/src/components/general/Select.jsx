// components
import SingleSelect from 'react-select'
import Creatable from 'react-select/creatable'

// styles
export const SelectStyle = {
  menuPortal: ({ left, top, ...provided }) => ({
    ...provided,
    right: left,
    top
  }),
  control: (provided) => ({
    ...provided,
    color: 'inherit',
    backgroundColor: 'none',
    border: 'none',
    boxShadow: 'none',
    cursor: 'pointer'
  }),
  option: (provided) => ({
    ...provided,
    cursor: 'pointer'
  })
}

export default function Select({ 
  options, 
  onChange, 
  value, 
  noCreatable, 
  noSearchable, 
  optionsNames, 
  ...restProps 
}) {

  const MySelect = noCreatable ? SingleSelect : Creatable
  const handleChange = data => {
    onChange(data?.value || '')
  }

  return <>
    <MySelect
      isRtl
      closeMenuOnScroll={() => true}
      menuPlacement='auto'
      placeholder=""
      styles={SelectStyle}
      components={{IndicatorSeparator: () => null}}
      options={options.map(option => ({
        value: option
      }))} 
      getOptionLabel={option => optionsNames?.[option.value] || option.value}
      noOptionsMessage={() => `אין בחירות${!noCreatable ? ', הכנס אופציה חדשה' : ''}`}
      onChange={handleChange}
      value={value?{value}:value}
      isSearchable={!noSearchable}
      isClearable={!restProps.required}
      theme={(theme) => ({
        ...theme,
        colors: {
        ...theme.colors,
          neutral80: 'inherit',
          primary25: '#2684ff2e',
          primary50: '#4c9aff69'
        },
      })}
      {...restProps}
    />
    {restProps.required && 
      <input
        tabIndex={-1}
        autoComplete="off"
        style={{ opacity: 0, height: 0, padding: 0, borderWidth: '1px', display: 'block' }}
        value={value}
        onChange={() => {}}
        required
      />
    }
  </>
}