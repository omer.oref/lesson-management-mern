// containers
import { Fade } from '@/containers/Fade'

// components
import ThemeSwitch from '@/components/general/ThemeSwitch'
import Auth from '@/components/admin/auth'

// styles
import './Footer.scss'

export default function Footer() {
  return (
    <Fade className="footer-con">
      <div className="footer">
        <div className="footer-text">
          <h3>ענף תוה"ד וחילוץ © 2022</h3>
          <p>פותח על ידי עומר כהן • צוות פל"א</p>
          <p>מומחה תוכן רועי פרילינג • מדור מפקדות ותחקור</p>
        </div>
        <ThemeSwitch />
        <Auth style={{ marginTop: '1em' }} />
      </div>
    </Fade>
  )
}