// containers
import { FadeLeft } from '@/containers/Fade'
import { FadeRight } from '@/containers/Fade'
import ImgLoader from '@/components/general/ImgLoader'

// images
import ArrowRight from '@/assets/arrow_right.svg'

// styles
import './BackTitle.scss'

export default function BackTitle({ text, showButton, handleClick }) {
  return (
    <h2 className="title-top back-title" style={{ margin: 0 }}>
    {!showButton && <FadeRight>{text}</FadeRight>}
    {showButton && (
      <FadeLeft>
        <button 
          onClick={handleClick}
        >
          <ImgLoader
            loaderSize="1.4em"
            src={ArrowRight} 
          />
        </button>
      </FadeLeft>
    )}
  </h2>
)
}