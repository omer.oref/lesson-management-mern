// components
import SingleSelect from 'react-select'
import Creatable from 'react-select/creatable'

// styles
import { SelectStyle } from '@/components/general/Select'

export const MultiSelectStyle = {
  multiValue: (provided) => ({
    ...provided,
    borderRadius: '.5em',
    padding: '.1em .2em',
    paddingInlineStart: '.4em'
  }),
  multiValueRemove: (provided) => ({
    ...provided,
    cursor: 'pointer',
    transition: 'color .2s, background-color .2s',
    borderRadius: '.4em',
    ':hover': {
      color: '#ff3300',
      backgroundColor: '#ff330017',
    }
  })
}

export default function MultiSelect({ 
  options, 
  onChange, 
  value, 
  noCreatable, 
  noSearchable, 
  optionsNames, 
  ...restProps 
}) {

  const MySelect = noCreatable ? SingleSelect : Creatable
  const handleChange = data => {
    onChange(data.length?data.map(i=>i.value):'')
  }

  return <>
    <MySelect 
      isMulti
      menuPlacement='auto'
      placeholder=""
      styles={{...SelectStyle, ...MultiSelectStyle}}
      components={{IndicatorSeparator: () => null}}
      options={options.map(option => ({
        value: option
      }))} 
      getOptionLabel={option => optionsNames?.[option.value] || option.value}
      noOptionsMessage={() => `אין בחירות${!noCreatable ? ', הכנס אופציה חדשה' : ''}`}
      onChange={handleChange}
      value={value?value.map(value=>({value})):value}
      isSearchable={!noSearchable}
      isClearable={!restProps.required}
      theme={(theme) => ({
        ...theme,
        colors: {
        ...theme.colors,
          neutral80: 'inherit',
          primary25: '#2684ff2e',
          primary50: '#4c9aff69'
        },
      })}
      {...restProps}
    />
    {restProps.required && 
      <input
        tabIndex={-1}
        autoComplete="off"
        style={{ opacity: 0, height: 0, padding: 0, borderWidth: '1px', display: 'block' }}
        value={value}
        onChange={() => {}}
        required
      />
    }
  </>
}