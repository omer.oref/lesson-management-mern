import { useState, useEffect, useRef } from "react"
import styled from "styled-components"

const Scroller = styled.div`
  cursor: pointer;
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 999;
  bottom: 2em;
  right: 2em;
  width: 4em;
  height: 4em;
  border-radius: 50%;
  background: #fff;
  box-shadow: 0 0 .5em #ddd;
  transition: opacity .2s;
  opacity: ${props => props.shouldShow ? 1 : 0};
  pointer-events: ${props => props.shouldShow ? 'auto' : 'none'};
`


export default function TopScroller() {

  const scrollerRef = useRef(null)
  const [shouldShow, setShouldShow] = useState(false)

  useEffect(() => {
    const parent = scrollerRef.current.parentElement
    parent.style.scrollBehavior = 'smooth'
    
    parent.addEventListener("scroll", () => {
      if (parent.scrollTop > 100) setShouldShow(true)
      else setShouldShow(false)
    })
  }, [])

  const handleClick = () => {
    const parent = scrollerRef.current.parentElement
    parent.scrollTop = 0
  }

  return (
    <Scroller className="darkmode-invert" ref={scrollerRef} shouldShow={shouldShow} onClick={handleClick}>
      <svg xmlns="http://www.w3.org/2000/svg" width="2em" height="2em" viewBox="0 0 24 24" strokeWidth="1.5" stroke="#333" fill="none" strokeLinecap="round" strokeLinejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <polyline points="7 11 12 6 17 11" />
        <polyline points="7 17 12 12 17 17" />
      </svg>
    </Scroller>
  )
}