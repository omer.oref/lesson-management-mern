// components
import ImgLoader from '@/components/general/ImgLoader'

// styles
import './ImgButton.scss'

export default function ImgButton({ img, ...restProps}) {
  return (
    <button 
      {...restProps}
      className={`img-button ${restProps.className || ''}`}
    >
      <ImgLoader loaderSize="1.3em" className="img-black" src={img} />
    </button>
  )
}