import { useEffect, isValidElement, useState } from "react"

// containers
import { Fade } from '@/containers/Fade'

export default function QueryLoader({ 
  children, 
  query, 
  LoadingIndicator, 
  ErrorIndicator,
  onError,
  EmptyIndicator,
  showWhenEmpty,
  showWhenNoData,
  ...restProps
}) {
  // must specify query
  if (!query) throw new Error('must specify query for QueryLoader')

  // query will always be an array
  if (!Array.isArray(query)) query = [query]

  let [isData, isLoading, error] = [null, false, null]

  const [isFirstLoad, setIsFirstLoad] = useState(null)
  useEffect(() => {
    if (isFirstLoad == null) setIsFirstLoad(isLoading)
  }, [isLoading])

  isData = showWhenNoData ? true : showWhenEmpty ? query.every(q => q.data) : query.every(q => queryHasData(q))
  isLoading = query.some(q => q.isLoading)
  error = query.find(q => q.error)?.error

  useEffect(() => {
    if (error && onError) onError(error)
  }, [error])

  return <>
    {isFirstLoad != null && isData && (
      isFirstLoad ? (
        <Fade {...restProps}>
          {children}
        </Fade>
      ) : children
    )}
    {isValidElement(ErrorIndicator)
      ? error && ErrorIndicator
      : error && ErrorIndicator && ErrorIndicator(error)
    }
    {isValidElement(LoadingIndicator)
      ? isLoading && LoadingIndicator
      : LoadingIndicator && LoadingIndicator(isLoading)
    }
    {isValidElement(EmptyIndicator)
      ? !isLoading && !isData && !error && EmptyIndicator
      : !isLoading && !isData && !error && EmptyIndicator && EmptyIndicator(error)
    }
  </>
}

const queryHasData = query => {
  if (Array.isArray(query.data)) {
    return !!query.data.length
  } else if (typeof query.data == 'object') {
    if (query.data.results) return !!query.data.results.length
    else if (query.data.pages) {
      if (Array.isArray(query.data.pages[0])) return !!query.data.pages[0].length
      if (typeof query.data.pages[0] == 'object') return !!query.data.pages[0].results?.length
    }
    else return !!query.data
  } else return !!query.data
}