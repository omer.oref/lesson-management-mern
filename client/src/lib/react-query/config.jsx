import toast from 'react-hot-toast'
import { stringify as qsStringify } from 'qs'
import { QueryClient, QueryClientProvider } from "react-query"

// query client
export const queryClient = new QueryClient({ 
  defaultOptions: {
    queries: {
      staleTime: 1000 * 5,
      notifyOnChangeProps: 'tracked',
      onError: err => {
        toast.error((err.response?.data?.message || err.message) || 'אירעה שגיאה')
      },
      retry: 0
    },
    mutations: {
      onError: err => {
        toast.error((err.response?.data?.message || err.message) || 'אירעה שגיאה')
      }
    }
  }
})

// key factory
export const keys = {
  areas: ['areas'],

  area: areaId => [...keys.areas, areaId],

  lessons: (areaId, { filters, page, limit }={}) => {
    const key = [...keys.area(areaId), 'lessons']
    const queryString = qsStringify({ filters, page, limit }, { encode: false })
    queryString && key.push('?'+queryString)
    return key
  },

  lesson: (areaId, lessonIndex) => [
    ...keys.area(areaId),
    'lessons',
    lessonIndex
  ],

  fields: areaId => [
    ...keys.area(areaId),
    'fields'
  ],

  stats: (areaId, { filters }={}) => {
    const key = [...keys.area(areaId), 'stats']
    const queryString = qsStringify({ filters }, { encode: false })
    queryString && key.push('?'+queryString)
    return key
  },

  fieldStats: (areaId, field, { filters }={}) => {
    const key = [...keys.area(areaId), 'fieldstats', field]
    const queryString = qsStringify({ filters }, { encode: false })
    queryString && key.push('?'+queryString)
    return key
  },

  fieldValues: (areaId, field, { filters }={}) => {
    const key = [...keys.area(areaId), 'fieldvalues']
    field && key.push(field)
    const queryString = qsStringify({ filters }, { encode: false })
    queryString && key.push('?'+queryString)
    return key
  },

  authStatus: ['auth']
}

// client provider
export function ReactQueryProvider({ children }) {
  return (
    <QueryClientProvider client={queryClient}>
      {children}
    </QueryClientProvider>
  )
}