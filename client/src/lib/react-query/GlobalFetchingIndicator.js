import { useEffect, useRef } from 'react'
import toast from 'react-hot-toast'
import { useIsFetching } from 'react-query'

const toastStyle = { background: 'none', boxShadow: 'none' }

export default function GlobalFetchingIndicator() {
  
  const isFetching = useIsFetching()
  const toastId = useRef()

  useEffect(() => {
    if (isFetching && !toastId.current) {
      toastId.current = toast.loading('', { style: toastStyle, position: 'top-right' })
    } else {
      toast.dismiss(toastId.current)
      toastId.current = null
    }
  }, [isFetching])
  
  return null
}