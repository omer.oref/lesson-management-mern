import { useParams } from 'react-router-dom'
import { useQuery, useInfiniteQuery } from 'react-query'
import { axiosInstance } from '@/lib/axios'
import { queryClient, keys } from './config'

// schemas
import { array } from 'yup' 
import { areaSchema, lessonSchema, fieldsSchema } from '@/models'

const isDev = import.meta.env.DEV
const devTimeout = 500
const sleep = ms => new Promise(resolve => setTimeout(resolve , ms))

//-- query function generator --//
const getQueryFn = (queryKey, {schema, select, isInfinite}={}) => async({pageParam=1}) => {
  // fake loads for development
  isDev && await sleep(devTimeout)
  isDev && queryKey.includes('fields') && await sleep(devTimeout)
  
  let data
  let url = ''
  queryKey.forEach(key => {
    url += key.charAt(0) == '?' ? key : `/${key}`
  })

  if (isInfinite) {
    url += queryKey[queryKey.length-1].charAt(0) == '?' 
    ? '&page='+pageParam
    : '?page='+pageParam
  }
  
  try {
    const res = await axiosInstance.get(url)
    data = res.data
  } catch (error) {
    console.error(error)
    throw new Error(error.response.data?.message || error.message)
  }
  try {
    if (schema) await schema.validate(data.results ?? data)
    return select ? select(data) : data
  } catch (error) {
    console.error(error)
    throw new Error('Data validation error')
  }
}

//-- areas --//
export const useAreas = () => {  
  const queryKey = keys.areas

  return useQuery(
    queryKey,
    getQueryFn(queryKey, {
      schema: array(areaSchema)
    })
  )
}

export const useArea = () => {
  const { areaId } = useParams()
  const queryKey = keys.area(areaId)
  
  return useQuery(
    queryKey, 
    getQueryFn(queryKey, {
      schema: areaSchema
    }),
    { 
      initialData: () => {
        return queryClient
        .getQueryData(keys.areas)
        ?.find(area => area.id == areaId)
      }
    }
  )
}

//-- lessons --//
export const useLessons = ({ filters, page, limit }={}) => {
  const fixedFilters = filters?.drill 
    ? {...filters, 'drill.name': filters?.drill, drill: undefined}
    : filters

  const { areaId } = useParams()
  const queryKey = keys.lessons(areaId, { filters: fixedFilters, page, limit })

  return useInfiniteQuery(
    queryKey,
    getQueryFn(queryKey, {
      schema: array(lessonSchema), 
      isInfinite: true
    }), 
    {
     getNextPageParam: lastPage => lastPage.meta?.nextPage
    }
  )
}

export const useLesson = lessonIndex => {
  const { areaId } = useParams()
  const queryKey = keys.lesson(areaId, lessonIndex)

  return useQuery(
    queryKey, 
    getQueryFn(queryKey, {
      schema: lessonSchema
    }),
    {
      initialData: () => {
        const lessons = queryClient.getQueriesData(
          keys.lessons(areaId)
        )?.find(q => q[0][q[0].length-1].charAt(0)=='?' || q[0][q[0].length-1]=='lessons')
        let initialLesson = undefined
        lessons?.[1]?.pages.forEach(page => page.results.forEach(lesson => {
          if (lesson.index == lessonIndex) {
            initialLesson = lesson; return
          }
        }))
        return initialLesson
      }
    }
  )
}

//-- fields --//
export const useFields = querySettings => {
  const { areaId } = useParams()
  const queryKey = keys.fields(areaId)

  return useQuery(
    queryKey,
    getQueryFn(queryKey, {
      schema: fieldsSchema, 
      select: results => results.sort((a,b) => a.index-b.index)
    }),
    querySettings
  )
}

//-- stats --//
export const useStats = ({ filters }={}, querySettings) => {
  const fixedFilters = filters?.drill 
    ? {...filters, 'drill.name': filters?.drill, drill: undefined}
    : filters

  const { areaId } = useParams()
  const queryKey = keys.stats(areaId, { filters: fixedFilters })

  return useQuery(
    queryKey,
    getQueryFn(queryKey),
    querySettings
  )
}

export const useFieldStats = (field, { filters }={}, querySettings) => {
  const fixedFilters = filters?.drill 
    ? {...filters, 'drill.name': filters?.drill, drill: undefined}
    : filters

  const { areaId } = useParams()
  const queryKey = keys.fieldStats(areaId, field, { filters: fixedFilters })

  return useQuery(
    queryKey,
    getQueryFn(queryKey),
    querySettings
  )
}

//-- unique values --//
export const useFieldValues = (field, { filters }={}, querySettings) => {
  const fixedFilters = filters?.drill 
    ? {...filters, 'drill.name': filters?.drill, drill: undefined}
    : filters

  if (!Array.isArray(field)) field = [field]

  const { areaId } = useParams()
  const queryKey = keys.fieldValues(areaId, field.join(','), { filters: fixedFilters })

  return useQuery(
    queryKey,
    getQueryFn(queryKey),
    querySettings
  )
}


// -- auth --//
export const useAuthStatus = querySettings => {
  const queryKey = keys.authStatus

  return useQuery(
    queryKey,
    getQueryFn(queryKey),
    querySettings
  )
}