// styles
import './Text.scss'

export default function AreaPickerPage_text() {
  return (
    <div className="area_page-text">
      <h1>מנה"ל</h1>
      <h3>מערכת ניהול הלקחים של פיקוד העורף</h3>
      <p>
        ברוכים הבאים למערכת ניהול הלקחים בפיקוד העורף - מנה"ל <br />
        המערכת מנוהלת ע"י מדור מפקדות ותחקור - ענף תוה"ד וחילוץ <br />
        המערכת הוקמה בשנת 2022
      </p>  
    </div>
  )
}
