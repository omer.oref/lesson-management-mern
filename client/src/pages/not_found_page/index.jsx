import styled from 'styled-components'

// containers
import { FadeDown } from '@/containers/Fade'

// images
import NotFoundImg from '@/assets/notfound.svg'

const PageStyled = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export default function NotFoundPage() {
  return (
    <PageStyled>
      <h2 className="title-top">
        עמוד לא נמצא
      </h2>
      <FadeDown>
        <img src={NotFoundImg} style={{
          width: '40em',
          padding: '5em 2em',
          maxWidth: '100%'
        }} />
      </FadeDown>
    </PageStyled>
  )
}
