import styled from 'styled-components'

// queries 
import { useFieldValues } from '@/lib/react-query/queries'

// utils
import { getLastDrill, getLastYear } from '@/utils/lessonsUtils'

// components
import Pie from '@/components/lesson-stats/Pie'
import PieSkeleton from '@/components/lesson-stats/components/PieSkeleton'

// styles
const PiesCon = styled.div`
  margin: 0 auto;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  gap: 5vmax;
`

export default function StatsPagePies() {

  const drillsQuery = useFieldValues('drill')
  const yearsQuery = useFieldValues('dateToFinish.year')
  
  const lastDrill = getLastDrill(drillsQuery.data)
  const lastYear = getLastYear(yearsQuery.data)
  
  return (
    <PiesCon>
      <Pie 
        title={<span>התפלגות סטטוס יישום של <b>כלל הלקחים במערכת</b></span>}
      />
      {lastDrill ? (
        <Pie 
          title={<span>התפלגות סטטוס יישום <b>כלל הלקחים מהאירוע האחרון ({lastDrill.name || 'לא מוגדר'})</b></span>}
          filters={{ 'drill.id': lastDrill.id }}
        />
      ) : <PieSkeleton />}
      {lastYear != undefined ? (
        <Pie 
          title={<span>התפלגות סטטוס יישום <b>כלל הלקחים מהשנה האחרונה ({lastYear || 'לא מוגדר'})</b></span>}
          filters={{ 'dateToFinish.year': lastYear }}
        />
      ) : <PieSkeleton />}
    </PiesCon>
  )
}
