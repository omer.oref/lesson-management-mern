import styled from 'styled-components'

// components
import Pies from './Pies'
import Graphs from './Graphs'

// containers
import { FadeDown } from '@/containers/Fade'

// styles
const StatsPageCon = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-rows: auto 1fr;
`
  
const Content = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  place-items: center;
`

export default function StatsPage() {
  return (
    <StatsPageCon>
      <h2 className="title-top">תמונת מצב לקחים</h2>
      <FadeDown>
        <Content>
          <Pies />
          <FadeDown>
            <Graphs />
          </FadeDown>
        </Content>
      </FadeDown>
    </StatsPageCon>
  )
}
