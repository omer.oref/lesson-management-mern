import styled from 'styled-components'

// components
import Graph from '@/components/lesson-stats/Graph'

// styles
const GraphsCon = styled.div`
  margin: 2em auto;
  width: 100%;
  maxWidth: 100vw;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  gap: 5vmax;
`

const graphStyle = {
  width: '35em',
  height: '25em',
  maxWidth: '95vw',
}

export default function StatsPageGraphs() {
  return (
    <GraphsCon>
      <Graph 
        title={<span>שלושת בעלי התחום בעלי אחוז הלקחים המיושמים <b>הנמוך ביותר</b></span>} 
        dataKey="responsibility" 
        style={graphStyle}
        selectFn={segments => segments.slice(0,3)}
      />
      <Graph 
        title={<span>שלושת בעלי התחום בעלי אחוז הלקחים המיושמים <b>הגבוה ביותר</b></span>} 
        dataKey="responsibility" 
        style={graphStyle}
        selectFn={segments => segments.slice(-3)}
      />
    </GraphsCon>
  )
}