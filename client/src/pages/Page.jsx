import { useEffect } from "react"

// utils
import setAppHue from '@/utils/setAppHue'

// queries
import { useArea } from '@/lib/react-query/queries'

// components
import { FadeDown } from '@/containers/Fade'

export default function Page({ Component, title }) {

  const { data: area } = useArea()
  
  // set document title
  useEffect(() => {
    document.title = title
  },[])

  // update app hue
  useEffect(() => {
    area && setAppHue(area.hue)
  }, [area])


  return (
    <FadeDown style={{ height: '100%', width: '100%' }}>
      <div className="page">
        <Component />
      </div>
    </FadeDown>
  )
}