// router
import { Link } from 'react-router-dom'

//containers
import ImgLoader from '@/components/general/ImgLoader'

// images
import Spreadsheets from '@/assets/spreadsheet.svg'
import Data from '@/assets/data.svg'
import Personal from '@/assets/personal.svg'

// nav items
const items = [
  { img: Personal, text: 'הלקחים שלי', to: 'me' },
  { img: Spreadsheets, text: 'טבלת לקחים', to: 'table' },
  { img: Data, text: 'תמונת מצב לקחים', to: 'stats' }
]

export default function Nav() {
  return (
    <div className="nav-panel">
      {items.map(item => (
        <Link to={item.to} className="item" key={item.text}>
          <ImgLoader src={item.img} loaderHeight="12em" loaderWidth="20em" />
          <div className="sep-line"></div>
          <p>{item.text}</p>
        </Link>
      ))}
    </div>
)
}