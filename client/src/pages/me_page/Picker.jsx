// queries
import { useFieldValues } from '@/lib/react-query/queries'

// components
import Picker from '@/components/general/Picker'
import EmptyIndicator from '@/components/general/EmptyIndicator'
import ErrorAlert from '@/components/general/ErrorAlert'
import Skeleton from "@mui/material/Skeleton"

// containers
import QueryLoader from '@/containers/QueryLoader'

// images
import PersonImg from '@/assets/person.svg'

export default function MePagePicker({ setPicked }) {

  const query = useFieldValues('responsibility')

  const handleClick = option => {
    setPicked(option)
  }
  
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', paddingTop: '1em'}}>
    <QueryLoader 
      query={query}
      LoadingIndicator={
        <div style={{ display: 'flex', margin: 'auto' }}>
          <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" style={{ margin: '.5em' }} />
          <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" style={{ margin: '.5em' }} />
          <Skeleton animation="wave" variant="rectangular" height="2em" width="6em" style={{ margin: '.5em' }} />
        </div>
      }
      EmptyIndicator={<EmptyIndicator text="אין בחירות" />}
      ErrorIndicator={error => <ErrorAlert message={error?.message} />}
    >

      <Picker options={query.data} handleClick={handleClick} getIconImg={() => PersonImg} />
    </QueryLoader>
    </div>
  )
}