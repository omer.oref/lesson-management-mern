import styled from 'styled-components'

// custom hooks
import useUrlParam from '@/hooks/useUrlParam'

// containers
import { FadeDown } from '@/containers/Fade'

// components
import Picker from './Picker'
import Stats from './Stats'
import BackTitle from '@/components/general/BackTitle'

// styles
const MePageCon = styled.div`
  width: 100%;
`

export default function MePage() {

  const [picked, setPicked] = useUrlParam('picked')

  return (
    <MePageCon>
      <BackTitle 
        text="הלקחים שלי"
        showButton={picked}
        handleClick={() => setPicked('')}
      />
      {!picked && <FadeDown><Picker setPicked={setPicked} /></FadeDown> }
      {picked && <FadeDown><Stats picked={picked} /></FadeDown>}
    </MePageCon>
  )
}
