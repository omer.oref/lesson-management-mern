import styled from "styled-components"

// custom hooks
import useUrlParam from '@/hooks/useUrlParam'

// containers
import { FadeDown } from '@/containers/Fade'

// components
import FilterPicker from "./FilterPicker"
import Stats from "./Stats"
import Picker from '@/components/general/Picker'
import BackTitle from '@/components/general/BackTitle'

// images
import DateImg from '@/assets/date.svg'
import PlaceImg from '@/assets/place.svg'

// styles
const FilterPageCon = styled.div`
  width: 100%;
`

const filters = {
  ['אירוע']: { dataKey: 'drill.name' },
  ['שנה']: { dataKey: 'dateToFinish.year' },
}

export default function FilterPage() {

  const [type, setType] = useUrlParam('type')
  const [picked, setPicked] = useUrlParam('picked')

  const handleBack = () => {
    if (picked) {
      setPicked('')
    } else if (type) {
      setType('')
    }
  }

  return (
    <FilterPageCon>
      <BackTitle 
        text="מיון אירוע\שנה"
        showButton={type || picked}
        handleClick={handleBack}
      />
      {!type && !picked && (
        <FadeDown style={{ paddingTop: '1em' }}>
          <Picker 
            options={['אירוע', 'שנה']}
            handleClick={option => setType(option)}
            getIconImg={option => 
              option == 'שנה' ? DateImg 
              : option == 'אירוע' ? PlaceImg 
              : null
            }
          />
        </FadeDown>
      )}
      {type && !picked && (   
        <FadeDown style={{ paddingTop: '1em' }}>
          <FilterPicker 
            type={type} 
            dataKey={filters[type].dataKey} 
            setPicked={setPicked} 
          />
        </FadeDown>
      )}
      {type && picked && (
        <FadeDown>
          <Stats 
            dataKey={filters[type].dataKey}
            value={picked} 
          />
        </FadeDown>
      )}
    </FilterPageCon>
  )
}
